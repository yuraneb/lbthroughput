package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

var url = "http://speedtest.dal01.softlayer.com/downloads/test100.zip"
var bufferSize = 1000000 // 1MB buffer
const bytesInMB = 1048576

func main() {

	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	} else {
		defer response.Body.Close()
		countingRead(response.Body)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func countingRead(r io.Reader) uint64 {
	buf := make([]byte, bufferSize)
	var byteCount uint64

	for {
		n, err := io.ReadFull(r, buf)
		byteCount += uint64(n)
		fmt.Printf("Read %v bytes\n", n)
		if err != nil {
			fmt.Println("Done reading!")
			break
		}
	}

	fmt.Printf("Read total %v MB\n", byteCount/bytesInMB)
	return byteCount
}
