# lbthroughput : load balancer throughput benchmarking tool

## Overview

The purpose of this project is to benchmark the throughput performance of load balancers (currently only DO load balancers are supported).
The benchmarking framework deploys a load balancer, a group of servers behind the load balancer, and a group of external clients.
Note that we're not trying to test the aggregate number of HTTP requests that can be supported, or gauge the performance of the HTTP router your project uses,
there are many other (great) tools for that. The focus of this project is solely *maximum throughput capacity*.

## Deployment and Usage
